from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.core.context_processors import csrf

from simulatore.models import estrazione, quote
from simulatore.forms import Scommessa
import json

def calcola(val):
    if val == 1:
        return 9999
    else:
        return int(val)

def quanto_guadagno(num,data):
    guadagno=0
    pq = quote.objects.filter(quando=data)[0]
    if num >= 3:
        guadagno+=calcola(pq.tre)
    if num >= 4:
        guadagno+=calcola(pq.qua)
    if num >= 5:
        guadagno+=calcola(pq.cin)
    if num >= 6:
        guadagno+=calcola(pq.cpu)
        guadagno+=calcola(pq.sei)
    return guadagno


def get_vincite(myset):
    res={ '3':0, '4':0, '5':0, '51':0, '6':0, 'guadagno':0,'spesa':0 }
    for tt in estrazione.objects.all():
        res['spesa']+=1
        est = set([tt.num1.to_eng_string(),
            tt.num2.to_eng_string(),
            tt.num3.to_eng_string(),
            tt.num4.to_eng_string(),
            tt.num5.to_eng_string(),
            tt.num6.to_eng_string(),
            tt.jolly.to_eng_string()])
        combo=myset.intersection(est)
        if len(combo) >= 3:
            pq = quote.objects.filter(quando=tt.quando)[0]
            #res['3']+=1
            #res['guadagno']+=calcola(pq.tre)
            if len(combo) >= 4:
                res['4']+=1
                res['guadagno']+=calcola(pq.qua)
            if len(combo) >= 5:
                res['5']+=1
                res['guadagno']+=calcola(pq.cin)
            if len(combo) >= 6:
                if tt.jolly.to_eng_string() in combo:
                    res['51']+=1
                    res['guadagno']+=calcola(pq.cpu)
                res['6']+=1
                res['guadagno']+=calcola(pq.sei)
    res['done'] = 1
    if res['guadagno']== 0:
        res['vinto'] = 0
    else:
        res['vinto'] = 1
    res['spesa']=res['spesa']/2
    return res

    

def index(request):
    c = {}
    val=[]
    c.update(csrf(request))
    c['form'] = Scommessa()
    if request.method == "POST":
        form = Scommessa(request.POST)
        if not form.is_valid():
            return HttpResponse(json.dumps({
                    'status': 'KO',
                    'msg':form.errors}),
                    mimetype = "application/json")
        else:
            for tt in ['pr','se','te','qa','qi','ss','jo']:
                val.append(str(form.cleaned_data[tt]))
            res=get_vincite(set(val))
            c['form'] = form
            c['res'] = res 
    return render(request, 'index.html',c)
