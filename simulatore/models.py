from django.db import models

class estrazione(models.Model):
    quando = models.DateTimeField('data estrazione')
    num1 = models.DecimalField(max_digits=5, decimal_places=2)
    num2 = models.DecimalField(max_digits=5, decimal_places=2)
    num3 = models.DecimalField(max_digits=5, decimal_places=2)
    num4 = models.DecimalField(max_digits=5, decimal_places=2)
    num5 = models.DecimalField(max_digits=5, decimal_places=2)
    num6 = models.DecimalField(max_digits=5, decimal_places=2)
    jolly = models.DecimalField(max_digits=5, decimal_places=2)
    sstar = models.DecimalField(max_digits=5, decimal_places=2)
    def __unicode__(self):
        return '%s'%self.quando.strftime('%Y/%m/%d')

class quote(models.Model):
    quando = models.DateTimeField('data estrazione')
    sei = models.DecimalField(max_digits=15, decimal_places=2)
    cpu = models.DecimalField(max_digits=15, decimal_places=2)
    cin = models.DecimalField(max_digits=15, decimal_places=2)
    qua = models.DecimalField(max_digits=15, decimal_places=2)
    tre = models.DecimalField(max_digits=15, decimal_places=2)
    def __unicode__(self):
        return '%s'%self.quando.strftime('%Y/%m/%d')

class Scommessa(models.Model):
    num1 = models.DecimalField(max_digits=3, decimal_places=1)
    num2 = models.DecimalField(max_digits=3, decimal_places=1)
    num3 = models.DecimalField(max_digits=3, decimal_places=1)
    num4 = models.DecimalField(max_digits=3, decimal_places=1)
    num5 = models.DecimalField(max_digits=3, decimal_places=1)
    num6 = models.DecimalField(max_digits=3, decimal_places=1)
    jolly = models.DecimalField(max_digits=3, decimal_places=1)
    ##sstar = models.DecimalField(max_digits=3, decimal_places=1)
    def __unicode__(self):
        return '%d %d %d %d %d %d %d'%(self.num1,self.num2,self.num3,self.num4,self.num5,self.num6,self.jolly)
    
