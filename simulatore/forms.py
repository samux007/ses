from django import forms
from django.forms import widgets


attrs = {
    'size':2,
    'class':'nn',
    'onblur':"testField(this);", }

class Scommessa(forms.Form):
    pr = forms.CharField(
            max_length=3,
            widget=forms.TextInput(attrs=attrs),
            required=True,
            label=""
    )
    se = forms.CharField(max_length=3,widget=forms.TextInput(attrs=attrs),label="")
    te = forms.CharField(max_length=3,widget=forms.TextInput(attrs=attrs),label="")
    qa = forms.CharField(max_length=3,widget=forms.TextInput(attrs=attrs),label="")
    qi = forms.CharField(max_length=3,widget=forms.TextInput(attrs=attrs),label="")
    ss = forms.CharField(max_length=3,widget=forms.TextInput(attrs=attrs),label="")
    jo = forms.CharField(max_length=3,widget=forms.TextInput(attrs=attrs),label="")
    ##sstar = forms.CharField(max_length=3, decimal_places=1,label='st',widget=forms.TextInput())
