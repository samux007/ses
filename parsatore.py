#!/public/varie/bin/python

import os
from bs4 import BeautifulSoup
import requests

os.environ['PYTHONPATH'] = '/public/varie/enalotto'
os.environ['DJANGO_SETTINGS_MODULE'] = 'enalotto.settings'
from simulatore.models import *
from django.utils.timezone import utc
import datetime

def getdata(url):
    r = requests.get(url)
    if r.status_code == 200:
        data = r.text
        soup = BeautifulSoup(data)
        return soup
    return None

def archivio():
    ae=[]
    y=datetime.datetime.now().year
    while y>1996:
        p=1
        while p<14:
            ae.append('http://www.superenalotto3000.it/page/archivio_estrazioni.asp?page=%s&anno=%s'%(p,y))
            p+=1
        y-=1
    return ae
  
def estrazioni():
    dlist=[]
    for url in archivio():
        soup = getdata(url)
        if soup:
            ultima = estrazione.objects.extra(order_by = ['-quando'])[0]
            for kk in soup("a"):
                if 'estrazione.asp?data' in kk['href']:
                    dt=kk.text
                    ddt = datetime.datetime.strptime(dt,'%d/%m/%Y')
                    if ddt > ultima.quando:
                        dlist.append([dt,'http://www.superenalotto3000.it/page/estrazione.asp?data=%s'%dt])
                        print dt
    return dlist


def concorso(url):
    soup = getdata(url)
    numb=[]
    quota={}
    vals={'jolly':0,'superstar':0, 'quota':''}
    if soup:
        #estrazione 
        for num in soup.find_all(attrs={'class':'numeri'}):
            numb.append(int(num.text))
        vals['estrazione'] = numb
        for num in soup.find_all(attrs={'class':'jolly'}):
            if num:
                vals['jolly'] = num.text
        for num in soup.find_all(attrs={'class':'superstar'}):
            if num:
                vals['superstar'] = num.text
        #quote
        table = soup.find(attrs={'class':'table-quote'})
        vallist =  table.find_all('td')
        y=0
        while len(vallist) > 2+y:
            if "-" in vallist[2+y].text:
                quota[vallist[0+y].text] = 1
            else:
                val = vallist[2+y].text.split(' ')[0]
                val = val.replace('.','')
                val = val.replace(',','.')
                try:
                    val = float(val)
                except Exception,e:
                    print val
                    print e
                    val = 1
                quota[vallist[0+y].text] = val
            y=y+3
        vals['quota'] = quota
        return vals
    else:
        return None
            
def creaglioggetti(quando,dati):
    est=num['estrazione']
    estrazione.objects.create(quando=data, num1=est[0], num2=est[1], num3=est[2], num4=est[3], num5=est[4], num6=est[5],jolly=num['jolly'],sstar=num['superstar'])
    sei = num['quota'].get('6')
    cpu = num['quota'].get('5+1')
    cin = num['quota'].get('5')
    qua = num['quota'].get('4')
    tre = num['quota'].get('3')
    quote.objects.create(quando=data,sei=sei,cpu=cpu,cin=cin,qua=qua,tre=tre)

for val in estrazioni():
    num = concorso(val[1])
    if num:
        data = datetime.datetime.strptime(val[0],'%d/%m/%Y')
        creaglioggetti(data,num)
